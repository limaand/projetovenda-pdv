module.exports.usuario = function (application, req, res) {
	var connection = application.config.dbConnection();
	var usuarioModel = new application.app.models.UsuarioDAO(connection);

	usuarioModel.getUsuarios(function (error, result) {
		res.render("usuario/list", { usuarios: result, nomeUser: req.session.nomeUsuario});
	});


}


module.exports.add = function (application, req, res) {
	res.render('usuario/add', { validacao: {}, usuario: {}, nomeUser: req.session.nomeUsuario });
}


module.exports.usuario_salvar = function (application, req, res) {
	var usuario = req.body;

	req.assert('nome', 'nome é obrigatório').notEmpty();
	req.assert('login', 'login é obrigatório').notEmpty();
	req.assert('senha', 'senha é obrigatório').notEmpty();

	var erros = req.validationErrors();

	if (erros) {
		res.render("usuario/add", { validacao: erros, usuario: usuario, nomeUser: req.session.nomeUsuario });
		return;
	}

	var connection = application.config.dbConnection();
	var usuarioModel = new application.app.models.UsuarioDAO(connection);

	usuarioModel.salvarUsuario(usuario, function (error, result) {
		res.redirect('/usuario');
	});
}


module.exports.usuarioById = function (application, req, res) {
	var connection = application.config.dbConnection();
	var usuarioModel = new application.app.models.UsuarioDAO(connection);
	var usuario = req.params.id;

	usuarioModel.getUsuarioById(usuario, function (error, result) {
		res.render("usuario/edit", { validacao: {}, usuario: result, nomeUser: req.session.nomeUsuario });
	});
}



module.exports.usuario_delete = function (application, req, res) {
	var connection = application.config.dbConnection();
	var usuarioModel = new application.app.models.UsuarioDAO(connection);
	var usuario = req.params.id;

	usuarioModel.deleteUsuario(usuario, function (error, result) {
		if (error)
			console.log("Error Updating : %s ", error);
		  res.redirect('/usuario');
	});
}





module.exports.usuario_editar = function (application, req, res) {

	var usuario = req.body;

	req.assert('nome', 'nome é obrigatório').notEmpty();
	req.assert('login', 'login é obrigatório').notEmpty();
	req.assert('senha', 'senha é obrigatório').notEmpty();


	var erros = req.validationErrors()

	if (erros) {
		res.render("usuario/edit", { validacao: erros, usuario: usuario, nomeUser: req.session.nomeUsuario });
		return;
	}

		var connection = application.config.dbConnection();
		var usuarioModel = new application.app.models.UsuarioDAO(connection);
		var usuarioId = req.params.id;

		usuarioModel.editarUsuario(usuarioId, usuario, function (error, result) {
			if (error)
				console.log("Error Updating : %s ", error);
	
			res.redirect('/usuario');
		});

}
