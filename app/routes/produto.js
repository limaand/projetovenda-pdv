module.exports =  function(application){
    application.get('/produto', function(req, res){
         application.app.controllers.produto.produto(application, req, res);
    });

    application.get('/add', function(req, res){
        application.app.controllers.produto.add(application, req, res);
   });

   application.post('/produto/salvar', function(req, res){
    application.app.controllers.produto.produto_salvar(application, req, res);
   });
   
   application.get('/edit/(:id)', function(req, res){
    application.app.controllers.produto.produtoById(application, req, res);
   });

   application.post('/edit/(:id)', function(req, res){
    application.app.controllers.produto.produto_editar(application, req, res);
   });


   application.get('/delete/(:id)', function(req, res){
    application.app.controllers.produto.produto_delete(application, req, res);
   });



}