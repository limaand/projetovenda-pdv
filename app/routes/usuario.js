module.exports =  function(application){
    application.get('/usuario', function(req, res){
         application.app.controllers.usuario.usuario(application, req, res);
    });

    application.get('/usuario/add', function(req, res){
        application.app.controllers.usuario.add(application, req, res);
   });

   application.post('/usuario/salvar', function(req, res){
    application.app.controllers.usuario.usuario_salvar(application, req, res);
   });
   
   application.get('/usuario/edit/(:id)', function(req, res){
    application.app.controllers.usuario.usuarioById(application, req, res);
   });

   application.post('/usuario/edit/(:id)', function(req, res){
    application.app.controllers.usuario.usuario_editar(application, req, res);
   });


   application.get('/usuario/delete/(:id)', function(req, res){
    application.app.controllers.usuario.usuario_delete(application, req, res);
   });



}