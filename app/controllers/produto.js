module.exports.produto = function (application, req, res) {

	var connection = application.config.dbConnection();
	var produtoModel = new application.app.models.ProdutoDAO(connection);

	produtoModel.getProdutos(function (error, result) {
		res.render("produto/list", { produtos: result ,nomeUser: req.session.nomeUsuario });
	});


}


module.exports.add = function (application, req, res) {
	res.render('produto/add', { validacao: {}, produto: {}, nomeUser: req.session.nomeUsuario });
}


module.exports.produto_salvar = function (application, req, res) {
	var produto = req.body;

	req.assert('titulo', 'Título é obrigatório').notEmpty();
	req.assert('descricao', 'Descrição é obrigatório').notEmpty();
	req.assert('preco', 'Preço é obrigatório').notEmpty();

	var erros = req.validationErrors();

	if (erros) {
		res.render("produto/add", { validacao: erros, produto: produto, nomeUser: req.session.nomeUsuario });
		return;
	}

	var connection = application.config.dbConnection();
	var produtoModel = new application.app.models.ProdutoDAO(connection);

	produtoModel.salvarProduto(produto, function (error, result) {
		res.redirect('/produto');
	});
}


module.exports.produtoById = function (application, req, res) {
	var connection = application.config.dbConnection();
	var produtoModel = new application.app.models.ProdutoDAO(connection);
	var produto = req.params.id;

	produtoModel.getProdutoById(produto, function (error, result) {
		res.render("produto/edit", { validacao: {}, produto: result, nomeUser: req.session.nomeUsuario });
	});
}



module.exports.produto_delete = function (application, req, res) {
	var connection = application.config.dbConnection();
	var produtoModel = new application.app.models.ProdutoDAO(connection);
	var produto = req.params.id;

	produtoModel.deleteProduto(produto, function (error, result) {
		if (error)
			console.log("Error Updating : %s ", error);
		  res.redirect('/produto');
	});
}





module.exports.produto_editar = function (application, req, res) {

	var produto = req.body;

	req.assert('titulo', 'Título é obrigatório').notEmpty();
	req.assert('descricao', 'Descrição é obrigatório').notEmpty();
	req.assert('preco', 'Preço é obrigatório').notEmpty();


	var erros = req.validationErrors()

	if (erros) {
		res.render("produto/edit", { validacao: erros, produto: produto });
		return;
	}

		var connection = application.config.dbConnection();
		var produtoModel = new application.app.models.ProdutoDAO(connection);
		var produtoId = req.params.id;

		produtoModel.editarProduto(produtoId, produto, function (error, result) {
			if (error)
				console.log("Error Updating : %s ", error);
	
			res.redirect('/produto', {nomeUser: req.session.nomeUsuario});
		});

}
