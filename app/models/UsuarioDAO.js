function UsuarioDAO(connection){
	this._connection = connection;
}

UsuarioDAO.prototype.getUsuarios = function(callback){
	this._connection.query('select * from usuario', callback);
}

UsuarioDAO.prototype.getUsuarioById = function(usuario, callback){
	this._connection.query('select * from usuario where id = ' + usuario, callback);
}


UsuarioDAO.prototype.editarUsuario = function(usuarioId, usuario, callback){
	console.log(usuarioId);
	console.log(usuario);
	this._connection.query('UPDATE usuario set ? WHERE id = '+usuarioId, [usuario], callback);
}



UsuarioDAO.prototype.salvarUsuario = function(usuario, callback){
	this._connection.query('insert into usuario set ? ', usuario, callback)
}


UsuarioDAO.prototype.deleteUsuario = function(usuario, callback){
	this._connection.query('DELETE FROM usuario WHERE id ='+usuario, callback)
}


UsuarioDAO.prototype.autenticar = function(usuario, callback){
	//console.log (usuario);
	//console.log([usuario.login]);
	this._connection.query('select * from usuario where login = ? and senha = ?', [usuario.login, usuario.senha], callback)
}




module.exports = function(){
	return UsuarioDAO;
}