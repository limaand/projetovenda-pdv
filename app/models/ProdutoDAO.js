function ProdutoDAO(connection){
	this._connection = connection;
}

ProdutoDAO.prototype.getProdutos = function(callback){
	this._connection.query('select * from produto', callback);
}

ProdutoDAO.prototype.getProdutoById = function(produto, callback){
	this._connection.query('select * from produto where id = ' + produto, callback);
}


ProdutoDAO.prototype.editarProduto = function(produtoId, produto, callback){
	console.log(produtoId);
	console.log(produto);
	this._connection.query('UPDATE produto set ? WHERE id = '+produtoId, [produto], callback);
}



ProdutoDAO.prototype.salvarProduto = function(produto, callback){
	this._connection.query('insert into produto set ? ', produto, callback)
}


ProdutoDAO.prototype.deleteProduto = function(produto, callback){
	this._connection.query('DELETE FROM produto WHERE id ='+produto, callback)
}

module.exports = function(){
	return ProdutoDAO;
}