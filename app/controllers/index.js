module.exports.index = function(application, req, res){
	res.render('index', {validacao:{}});
}



module.exports.autenticar = function(application, req, res){
	var dadosForm = req.body;
	req.assert('login', 'usuario não pode ser vazio').notEmpty();
	req.assert('senha', 'senha não pode ser vazio').notEmpty();
	
	var erros =  req.validationErrors();

	if(erros){
		res.render('index', {validacao:erros});
		return;
	}
	
	var connection = application.config.dbConnection();
	var UsuarioModel = new application.app.models.UsuarioDAO(connection);
	
	UsuarioModel.autenticar(dadosForm, function (error, result) {
 	  	if (error)
			console.log("Error SQL : %s ", error);
        if (result.length > 0){
			
			req.session.autorizado = true;
			req.session.nomeUsuario = result[0].nome;
			req.session.LoginUsuario = result[0].login;
     		
		};
		
        console.log(req.session.autorizado) 
		

		if (req.session.autorizado)
		   
		   res.render('painel',  {nomeUser: req.session.nomeUsuario});
		
		else
		res.render('index', {validacao:{}});
		
	});		

    

	

}